#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main(int argc, char *argv[]) {

    // get N from the command line
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    long long N = atoll(argv[1]);

    // calculate the sum
    long long sum = 0;
    for (long long i = 1; i <= N;i++) {
        sum += i;
    }

    // print the results
    printf ("sum = %lld\n",sum);
    printf ("N*(N+1)/2 = %lld\n",(N/2)*(N+1));
}
