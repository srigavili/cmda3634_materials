#include <stdio.h>

double vec_dot_prod (double u[], double v[], int dim) {
    double dot_prod = 0;
    for (int i=0;i<dim;i++) {
        dot_prod += u[i]*v[i];
    }
    return dot_prod;
}

int main () {
    int m = 4; // m - number of rows of A
    int n = 3; // n - number of columns of A
    double A[4][3] = { { 0.2, 1, 2 }, { 1, 2.5, 3 }, { 4, 5.8, 6 }, { 7, 8, 9.3 } };
    double x[3] = { 1, 1, 1 };
    double b[3];
    for (int i=0;i<m;i++) {
        b[i] = vec_dot_prod(A[i],x,n); // A[i] is a pointer to the ith row of A
    }
    printf ("b = { %.2f, %.2f, %.2f, %.2f }\n",b[0],b[1],b[2],b[3]);
}
