#include <stdio.h>

int main () {
    int m = 4; // m - number of rows of A
    int n = 3; // n - number of columns of A
    double A[4][3] = { { 0.2, 1, 2 }, { 1, 2.5, 3 }, { 4, 5.8, 6 }, { 7, 8, 9.3 } };
    double x[3] = { 1, 1, 1 };
    double b[4] = { 0 };
    for (int i=0;i<m;i++) {
        for (int j=0;j<n;j++) {
            b[i] += A[i][j]*x[j];
        }
    }
    printf ("b = { %.2f, %.2f, %.2f, %.2f }\n",b[0],b[1],b[2],b[3]);
}
