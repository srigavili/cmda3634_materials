#include <stdio.h>
#include <stdlib.h>

int main (int argc, char* argv[]) {

    // count number of primes <= n
    // where n is read from the command line
    if (argc < 2) {
        printf ("command usage: %s %s\n",argv[0],"n");
        return 1;
    }
    int n = atoi(argv[1]);

    // initially assume all integers >= 2 are prime
    // note that we ignore is_prime[0] and is_prime[1]
    int is_prime[n+1];
    for (int i = 2; i <= n; i++) {
        is_prime[i] = 1;
    }

    // mark non-primes <= n using Sieve of Eratosthenes
    for (int d=2;d*d<=n;d++) {
        // if d is prime, then mark multiples of d as non-prime
        // suffices to consider multiples d*d, d*d+d, d*d+2d, ...
        if (is_prime[d]) {
            for (int c=d*d;c<=n;c+=d) {
                is_prime[c] = 0;
            }
        }
    }

    // count primes
    int primes = 0;
    for (int i = 2; i <= n; i++) {
        if (is_prime[i]) {
            primes++;
        }
    }
    printf ("The number of primes <= %d is %d\n",n,primes);
}
