#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 5
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=4
#SBATCH -o mpi_sum.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules we need for MPI
module load matplotlib

# Build the executable
mpicc -o mpi_sum mpi_sum.c vec.c -lm

# Run mpi_sum
printf 'Results with one rank:\n'
mpiexec -n 1 --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_sum $1
printf '\nResults with multiple ranks:\n'
mpiexec -n $SLURM_NTASKS --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_sum $1

