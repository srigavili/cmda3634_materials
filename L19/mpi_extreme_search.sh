#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 00:10:00
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=32
#SBATCH -o mpi_extreme_search.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules we need for MPI
module load matplotlib

# Build the executable
mpicc -O3 -o mpi_extreme_approx mpi_extreme_approx.c vec.c -lm

# run mpi_work on nodes*ntasks-per-node cores
mpiexec -n $SLURM_NTASKS --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_extreme_approx $1 $2 $3

