#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main(int argc, char *argv[]) {

    // get N and num_threads from command line
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"N","num_threads");
        return 1;
    }
    long long N = atoll(argv[1]);
    int num_threads = atoi(argv[2]);
    omp_set_num_threads(num_threads);

    // start the timer
    double start_time, end_time;
    start_time = omp_get_wtime();

    // calculate the sum
    long long sum = 0;

    // private variable : each thread gets own copy
    // shared variable : one variable that all threads access
    // all variables declared in the parallel region are private
    // any other variable is shared

    // thread_num : private
    // i : private
    // sum : shared (read/write)
    // num_threads : shared (read only)
    // N : shared (read only)

    // A critical region can only be executed by one thread at a time
    // In this program we use the critical region to avoid read/write
    // race conditions on the shared variable sum.  

#pragma omp parallel default(none) shared(sum,num_threads,N)
    {
        int thread_num = omp_get_thread_num();
        for (long long i = 1+thread_num; i <= N;i+=num_threads) {
#pragma omp critical
            {
                sum += i;
            }
        }
    }

    // stop the timer
    end_time = omp_get_wtime();

    printf ("num_threads = %d, ",num_threads);
    printf ("elapsed time = %.6f seconds\n",end_time-start_time);
    printf ("sum = %lld\n",sum);
    printf ("N*(N+1)/2 = %lld\n",(N/2)*(N+1));
}
