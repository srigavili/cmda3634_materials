import sys
import numpy as np
import matplotlib.pyplot as plt

# load the points
data = np.loadtxt(sys.stdin,skiprows=1)

# set the aspect ratio to equal
plt.gca().set_aspect('equal')

# plot the centers (if additional command line arguments present)
if (len(sys.argv)-2 > 0):
    ctr = np.array([int(s) for s in sys.argv[2:]])
    dist_sq = np.zeros((len(ctr),len(data)))
    for i in range(len(ctr)):
        dist_sq[i] = np.sum((data-data[ctr[i]])*(data-data[ctr[i]]),axis=1)
    clu = np.argmin(dist_sq,axis=0)
    ex = np.argmax(np.min(dist_sq,axis=0))
    cost_sq = np.max(np.min(dist_sq,axis=0))
    plt.scatter (data[:,0],data[:,1],c=clu,cmap="tab10",s=10,alpha=0.5)
    plt.scatter (data[ctr,0],data[ctr,1],c=range(len(ctr)),cmap="tab10",s=100)
    plt.scatter (data[ex,0],data[ex,1],s=50,facecolors='none', edgecolors='black')
    plt.title ('Cost = %g' % np.sqrt(cost_sq))
else:
    plt.scatter (data[:,0],data[:,1],s=10,color='black')

#save the plot as an image
plt.savefig(sys.argv[1])
