import sys
import numpy as np
import time

# calculate the cost squared for centers with indices c1, c2
def calc_cost_sq(data,c1,c2):
    cost_sq = 0;
    n = len(data)
    for i in range(n):
        ds1 = np.inner(data[i]-data[c1],data[i]-data[c1])
        ds2 = np.inner(data[i]-data[c2],data[i]-data[c2])
        min_dist_sq = min([ds1,ds2])
        cost_sq = max([cost_sq,min_dist_sq])
    return cost_sq

# load the points
data = np.loadtxt(sys.stdin,skiprows=1)

# start the timer
tic = time.process_time()

# compute the minimal cost and an optimal solution
n = len(data)
min_cost_sq = float("inf")
tuples_checked = 0;
for c1 in range(0,n-1):
    for c2 in range(c1+1,n):
        tuples_checked += 1
        cost_sq = calc_cost_sq(data,c1,c2)
        if (cost_sq < min_cost_sq):
            min_cost_sq = cost_sq
            optimal_centers = np.array([c1,c2])

# stop the timer
toc = time.process_time()
elapsed = toc-tic

# print the results
print ('number of points =',n)
print ('2-tuples checked =',tuples_checked)
print ('elapsed time =',round(elapsed,2),'seconds')
print ('2-tuples checked per second =',int(tuples_checked/elapsed))
print ('minimum cost =',np.round(np.sqrt(min_cost_sq),2))
print ('optimal centers =',optimal_centers)
