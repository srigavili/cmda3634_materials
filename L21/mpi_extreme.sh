#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 5
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=4
#SBATCH -o mpi_extreme.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules we need for MPI
module load matplotlib

# Build the executable
mpicc -o mpi_extreme mpi_extreme.c vec.c -lm

# Run mpi_extreme
mpiexec -n $SLURM_NTASKS --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_extreme $1

