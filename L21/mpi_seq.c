#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>

int main(int argc, char *argv[]) {

    MPI_Init (&argc, &argv);

    // MPI_COMM_WORLD is the default communicator that contains all ranks
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    char node_name[MPI_MAX_PROCESSOR_NAME];
    int node_name_len;
    MPI_Get_processor_name(node_name,&node_name_len);

    // make sure we are not running on a login node!
    if ((strcmp(node_name,"tinkercliffs1") == 0) || 
            (strcmp(node_name,"tinkercliffs2") == 0)) {
        printf ("error : running on login node %s!\n",node_name);
        return 1;
    }

    // get the number of rounds from command line
    if (argc < 3) {
        printf ("Command usage : %s %s\n",argv[0],"rounds","seed");
        return 1;
    }
    int rounds = atoi(argv[1]);
    int seed = atoi(argv[2]);
    srandom(seed+rank);

    // start the timer
    double start_time, end_time;
    start_time = MPI_Wtime();

    // random sum
    int total_sum = 0;

    for (int round = 0;round < rounds;round++) {
	int round_sum = random() % 5;
	MPI_Status status;
	if (rank == 0) {
	    int number;
	    for (int src = 1;src < size;src++) {
		MPI_Recv(&number,1,MPI_INT,src,0,MPI_COMM_WORLD,&status);
		round_sum += number;
	    }
	} else {
	    int dest = 0;
	    MPI_Send(&round_sum,1,MPI_INT,dest,0,MPI_COMM_WORLD);
	}
	total_sum += round_sum;
    }

    // stop the timer
    end_time = MPI_Wtime();

    // print results
    if (rank == 0) {
        printf ("elapsed time = %.4f seconds\n",end_time-start_time);
	printf ("rounds = %d, seed = %d, sum = %d\n",rounds,seed,total_sum);
    }

    MPI_Finalize();
}
