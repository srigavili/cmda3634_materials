#include <stdio.h>
#include <stdlib.h>

// compute the total stopping time of a given start value
int total_stopping_time (long long start) {
    int total = 0;
    long long a_i = start;
    while (a_i != 1) {
        total += 1;
        if (a_i % 2 == 0) {
            a_i /= 2;
        } else {
            a_i = 3*a_i + 1;
        }
    }
    return total;
}

int main (int argc, char* argv[]) {

    // get N from the command line
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    long long N = atoll(argv[1]);

    // compute the start less than or equal to N with largest total stop
    long long max_start = 1;
    int max_total = 0;
    for (long long start=1; start <= N; start++) {
        int total = total_stopping_time (start);
        if (total > max_total) {
            max_total = total;
            max_start = start;
        }
    }

    // print results
    printf ("The starting value less than or equal to %lld\n",N);
    printf ("  having the largest total stopping time is %lld\n",max_start);
    printf ("  which has %d steps\n",max_total);

}
