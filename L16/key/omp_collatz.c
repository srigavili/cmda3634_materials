#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

// compute the total stopping time of a given start value
int total_stopping_time (long long start) {
    int total = 0;
    long long a_i = start;
    while (a_i != 1) {
        total += 1;
        if (a_i % 2 == 0) {
            a_i /= 2;
        } else {
            a_i = 3*a_i + 1;
        }
    }
    return total;
}

int main (int argc, char* argv[]) {

    // get N from the command line
    if (argc < 2) {
        printf ("Command usage : %s %s %s\n",argv[0],"N","num_threads");
        return 1;
    }
    long long N = atoll(argv[1]);
    int num_threads = atoi(argv[2]);
    omp_set_num_threads(num_threads);

    // start timer
    double start_time, end_time;
    start_time = omp_get_wtime();
    
    // compute the start less than or equal to N with largest total stop
    long long max_start = 1;
    int max_total = 0;
#pragma omp parallel default(none) shared(N,max_start,max_total)
    {
	long long thread_max_start = 1;
	int thread_max_total = 0;
#pragma omp for	schedule(dynamic,1000)
	for (long long start=1; start <= N; start++) {
	    int total = total_stopping_time (start);
	    if (total > thread_max_total) {
		thread_max_total = total;
		thread_max_start = start;
	    }
	}
#pragma omp critical
	{
	    if (thread_max_total > max_total) {
		max_total = thread_max_total;
		max_start = thread_max_start;
	    }
	}
    }

    // end timer
    end_time = omp_get_wtime();

    // print results
#ifdef TIMING
    printf ("(%d,%.4f),",num_threads,end_time-start_time);
#else    
    printf ("num_threads = %d, elapsed_time = %.2f\n",num_threads,
	    end_time-start_time);
    printf ("The starting value less than or equal to %lld\n",N);
    printf ("  having the largest total stopping time is %lld\n",max_start);
    printf ("  which has %d steps\n",max_total);
#endif    

}
