#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

int main (int argc, char* argv[]) {

    // get N from the command line
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"N","num_threads");
        return 1;
    }
    long long N = atol(argv[1]);
    int num_threads = atoi(argv[2]);
    omp_set_num_threads(num_threads);

    // start timer
    double start_time, end_time;
    start_time = omp_get_wtime();
    
    // compute the mean
    double sum = 0;
#pragma omp parallel
    {
	double thread_sum = 0;
#pragma omp for	
	for (long long i=1;i<=N;i++) {
	    thread_sum += i;
	}
#pragma omp critical
	{
	    sum += thread_sum;
	}
    }
    double mean = sum/N;

    // compute the variance
    double sum_diff_sq = 0;
#pragma omp parallel
    {
	double thread_sum_diff_sq = 0;
#pragma omp for		
	for (long long i=1;i<=N;i++) {
	    thread_sum_diff_sq += (i-mean)*(i-mean);
	}
#pragma omp critical
	{
	    sum_diff_sq += thread_sum_diff_sq;
	}
    }
    double variance = sum_diff_sq/N;

    // compute the standard deviation
    double stdev = sqrt(variance);

    // end timer
    end_time = omp_get_wtime();
    
    // print the results
#ifdef TIMING
    printf ("(%d,%.4f),",num_threads,end_time-start_time);
#else    
    printf ("num_threads = %d, elapsed_time = %.4f\n",num_threads,
	    end_time-start_time);
    printf ("computed standard deviation: %.4f, sqrt((N^2-1)/12): %.4f\n",
	    stdev,sqrt((N*N-1)/12.0));
#endif    
}
