# compile usig TIMING flag
gcc -DTIMING -O3 -o omp_stdev omp_stdev.c -fopenmp -lm

# run multiple times for scaling study
./omp_stdev 1000000000 1
./omp_stdev 1000000000 2
./omp_stdev 1000000000 4
./omp_stdev 1000000000 8
./omp_stdev 1000000000 16

# print new line
printf '\n'
