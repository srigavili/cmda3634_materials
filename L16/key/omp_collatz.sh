# compile usig TIMING flag
gcc -DTIMING -O3 -o omp_collatz omp_collatz.c -fopenmp

# run multiple times for scaling study
./omp_collatz 10000000 1
./omp_collatz 10000000 2
./omp_collatz 10000000 4
./omp_collatz 10000000 8
./omp_collatz 10000000 16

# print new line
printf '\n'
