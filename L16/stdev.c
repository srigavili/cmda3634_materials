#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main (int argc, char* argv[]) {

    // get N from the command line
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    long long N = atol(argv[1]);

    // compute the mean
    double sum = 0;
    for (long long i=1;i<=N;i++) {
        sum += i;
    }
    double mean = sum/N;

    // compute the variance
    double sum_diff_sq = 0;
    for (long long i=1;i<=N;i++) {
        sum_diff_sq += (i-mean)*(i-mean);
    }
    double variance = sum_diff_sq/N;

    // compute the standard deviation
    double stdev = sqrt(variance);

    // print the results
    printf ("computed standard deviation: %.4lf, sqrt((N^2-1)/12): %.4lf\n",
	    stdev,sqrt((N*N-1)/12.0));
}
