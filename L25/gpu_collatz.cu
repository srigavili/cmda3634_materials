#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <time.h>

typedef unsigned long long int uint64;

// compute the total stopping time of a given start value
__device__ uint64 total_stopping_time (uint64 start) {
    uint64 total = 0;
    uint64 a_i = start;
    while (a_i != 1) {
        total += 1;
        if (a_i % 2 == 0) {
            a_i /= 2;
        } else {
            a_i = 3*a_i + 1;
        }
    }
    return start + (total << 48);
}

__global__ void collatzKernel(uint64 N) {

    __shared__ uint64 max_total_start;

    int thread_num = threadIdx.x;
    int num_threads = blockDim.x;

    if (thread_num == 0) {
        max_total_start = 0;
    }
    __syncthreads();

    uint64 thread_max_total_start = 0;
    for (uint64 n = 1+thread_num;n<=N;n+=num_threads) {
        uint64 total_start = total_stopping_time(n);
        if (total_start > thread_max_total_start) {
            thread_max_total_start = total_start;
        }
    }
    atomicMax(&max_total_start,thread_max_total_start);
    __syncthreads();

    if (thread_num == 0) {
        // output the results
        printf ("The starting value less than or equal to %llu\n",N);
        printf ("  having the largest total stopping time is %llu\n",
                max_total_start & 0x0000ffffffffffff);
        printf ("  which has %llu steps\n",max_total_start >> 48);
    }
}

int main (int argc, char** argv) {

    // get N and num_threads from the command line
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"N","num_threads");
        return 1;
    }
    uint64 N = atol(argv[1]);
    int num_threads = atoi(argv[2]);
    printf ("num_threads = %d\n",num_threads);

    // start the timer
    clock_t start = clock();

    // launch kernel
    collatzKernel <<< 1, num_threads >>> (N);
    cudaDeviceSynchronize();

    // stop the timer
    clock_t stop = clock();
    double elapsed = (double)(stop-start)/CLOCKS_PER_SEC;
    printf ("elapsed time = %.2f seconds\n",elapsed);

}
