#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <time.h>

typedef unsigned long long int uint64;

// compute the total stopping time of a given start value
__device__ uint64 total_stopping_time (uint64 start) {
    uint64 total = 0;
    uint64 a_i = start;
    while (a_i != 1) {
        total += 1;
        if (a_i % 2 == 0) {
            a_i /= 2;
        } else {
            a_i = 3*a_i + 1;
        }
    }
    return start + (total << 48);
}

__global__ void collatzKernel(uint64 N) {
    uint64 thread_num = (uint64)blockIdx.x*blockDim.x + threadIdx.x;
    uint64 start = thread_num+1;
    if (start <= N) {
        uint64 total_start = total_stopping_time(start);
        if ((total_start >> 48) == 986) {
            printf ("starting value n = %llu has total stopping time 986\n",start);
        }
    }
}

int main (int argc, char** argv) {

    // B is the number of threads per block
    // we typically choose B to be a multiple of 32
    // the maximum value of B is 1024
    // get N and B from the command line
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"N","B");
        return 1;
    }
    uint64 N = atol(argv[1]);
    int B = atoi(argv[2]);

    // G is the number of thread blocks
    // the maximum number of thread blocks G is 2^31 - 1 = 2147483647
    // We choose G to be the minimum number of thread blocks to have at least N threads
    int G = (N+B-1)/B;
    printf ("N = %llu\n",N);
    printf ("threads per block B = %d\n",B);
    printf ("number of thread blocks G = %d\n",G);
    printf ("number of threads G*B = %llu\n",(uint64)G*B);

    // start the timer
    clock_t start = clock();

    // launch kernel
    collatzKernel <<< G, B >>> (N);
    cudaDeviceSynchronize();

    // stop the timer
    clock_t stop = clock();
    double elapsed = (double)(stop-start)/CLOCKS_PER_SEC;
    printf ("elapsed time = %.2f seconds\n",elapsed);
}
