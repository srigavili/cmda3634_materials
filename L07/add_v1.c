#include <stdio.h>

typedef struct vec2_s {
    double x,y;
} vec2;

void vec2_add (vec2 a, vec2 b, vec2 c) {
    c.x = a.x + b.x;
    c.y = a.y + b.y;
}

int main () {
    vec2 u = { 1.0, 2.0 };
    vec2 v = { 2.0, 3.0 };
    vec2 w = { 0, 0 };
    vec2_add(u,v,w);
    printf ("%.2f %.2f\n",w.x,w.y);
}
