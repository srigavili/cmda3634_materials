#include <stdio.h>
#include <stdlib.h>

int main (int argc, char* argv[]) {
    if (argc < 2) {
        printf ("command usage: %s %s\n",argv[0],"n");
        return 1;
    }
    int n = atoi(argv[1]);
    for (int i=0;i<n-1;i++) {
        for (int j=i+1;j<n;j++) {
            printf ("%d %d\n",i,j);
        }
    }
}
