#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <omp.h>

typedef unsigned char byte;

// calculates ||u-v||^2
// we accumulate the result using a C int to avoid overflow
int vec_dist_sq (byte u[], byte v[], int dim) {
    int dist_sq = 0;
    for (int i=0;i<dim;i++) {
        dist_sq += (u[i]-v[i])*(u[i]-v[i]);
    }
    return dist_sq;
}

// for each test vector find the nearest training vector
void omp_nearest(byte train[], int num_train, byte test[], int num_test, int nearest[], int dim, int num_threads) {
    omp_set_num_threads(num_threads);

    // start the timer
    double start_time, end_time;
    start_time = omp_get_wtime();
    
    for (int i=0;i<num_test;i++) {
	int min_dist_sq = INT_MAX;
	for (int j=0;j<num_train;j++) {
	    int dist_sq = vec_dist_sq(test+i*dim,train+j*dim,dim);
	    if (dist_sq < min_dist_sq) {
		min_dist_sq = dist_sq;
		nearest[i] = j;
	    }
	}
    }

    // stop the timer
    end_time = omp_get_wtime();
    printf ("Time to find nearest neighbors using OpenMP = %.4f seconds\n",end_time-start_time);

}
