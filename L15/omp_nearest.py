import sys
import numpy as np
import gzip
import ctypes as ct # for calling C from Python
lib = ct.cdll.LoadLibrary("./omp_nearest.so") # load OpenMP C nearest function

# make sure a command line argument for the number of test images is provided
if (len(sys.argv) < 3):
    print ('command usage :',sys.argv[0],'num_test','num_threads')
    exit(1)
num_test = int(sys.argv[1])
num_threads = int(sys.argv[2])
print ('number of digits classified =',num_test)
print ('number of threads =',num_threads)

# Opens MNIST training image set and stores it as a 60000 x 784 matrix
# There are 60000 images, each of which is 28 x 28 pixels
# Each image is stored as a 28x28 = 784 dimensional row vector in the matrix
f = gzip.open('train-images-idx3-ubyte.gz','r')
f.read(16) # skip file header
buf = f.read(60000*28*28)
data = np.frombuffer(buf,dtype=np.uint8)
train = data.reshape(60000,28*28)

# Opening and saving the 60000 training labels
f = gzip.open('train-labels-idx1-ubyte.gz','r')
f.read(8) #skip header
buf = f.read(60000)
train_labels = np.frombuffer(buf,dtype=np.uint8)

# Opens MNIST test image set and stores it as a 10000 x 784 matrix
# There are 10000 images, each of which is 28 x 28 pixels
# Each image is stored as a 28x28 = 784 dimensional row vector in the matrix
f = gzip.open('t10k-images-idx3-ubyte.gz','r')
f.read(16) # skip header
buf = f.read(10000*28*28)
data = np.frombuffer(buf,dtype=np.uint8)
test = data.reshape(10000,28*28)

# Opening and saving the 10000 test labels
f = gzip.open('t10k-labels-idx1-ubyte.gz','r')
f.read(8) #skip header
buf = f.read(10000)
test_labels = np.frombuffer(buf,dtype=np.uint8)

# find the nearest neighbors using OpenMP in C
nearest = np.zeros(num_test,dtype='uint32')
train_cptr = train.ctypes.data_as(ct.POINTER(ct.c_uint8))
test_cptr = test.ctypes.data_as(ct.POINTER(ct.c_uint8))
nearest_cptr = nearest.ctypes.data_as(ct.POINTER(ct.c_int32))
lib.omp_nearest(train_cptr,ct.c_int(len(train)),test_cptr,ct.c_int(num_test),
        nearest_cptr,ct.c_int(len(train[0])),ct.c_int(num_threads))

# count nearest neighbor classification errors
labels_diff = test_labels[:num_test] - train_labels[nearest]
classify_errors = np.count_nonzero(labels_diff)
print ('number of classification errors =',classify_errors)
print ('classificiation rate =',(num_test-classify_errors)/num_test)

