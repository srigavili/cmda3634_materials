#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include "vec.h"

int main (int argc, char* argv[]) {

    // get num_threads from the command line
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"num_threads");
        return 1;
    }
    int num_threads = atoi(argv[1]);
    omp_set_num_threads(num_threads);

    // read the number of points and the dimension of each point
    int num_points, dim;
    if (scanf("%*c %d %d",&num_points, &dim) != 2) {
        printf ("error reading the number of points and the dimension\n");
        return 1;
    }

    // read vectors from stdin and store them in a 2d array
    double* data = (double*)malloc(num_points*dim*sizeof(double));
    if (data == NULL) {
        printf ("malloc return NULL pointer!\n");
        return 1;
    }
    for (int i=0;i<num_points;i++) {
        if (vec_read_stdin(data+i*dim,dim) != dim) {
            printf ("error reading the next point from stdin\n");
            return 1;
        }
    }

    // start the timer
    double start_time, end_time;
    start_time = omp_get_wtime();

    // find the extreme pair
    double max_dist_sq = 0;
    int extreme[2];
    int pairs_checked = 0;

#pragma omp parallel default(none) shared(dim,data,pairs_checked,max_dist_sq,extreme,num_points,num_threads)
    {
        double thread_max_dist_sq = 0;
        int thread_extreme[2];
        int thread_pairs_checked = 0;
        int thread_num = omp_get_thread_num();
#ifdef DYNAMIC
#pragma omp for schedule(dynamic)
#else 
#pragma omp for schedule(static)
#endif
        for (int i=0;i<num_points-1;i++) {
            for (int j=i+1;j<num_points;j++) {
                double dist_sq = vec_dist_sq(data+i*dim,data+j*dim,dim);
                thread_pairs_checked += 1;
                if (dist_sq > thread_max_dist_sq) {
                    thread_max_dist_sq = dist_sq;
                    thread_extreme[0] = i;
                    thread_extreme[1] = j;
                }
            }
        }
#ifdef DEBUG
        printf ("thread %d checked %d pairs\n",thread_num,thread_pairs_checked);
#endif
#pragma omp critical
        {
            pairs_checked += thread_pairs_checked;
            if (thread_max_dist_sq > max_dist_sq) {
                max_dist_sq = thread_max_dist_sq;
                extreme[0] = thread_extreme[0];
                extreme[1] = thread_extreme[1];
            }
        }

    }

    // stop the timer
    end_time = omp_get_wtime();

    // output the results
    printf ("num_threads = %d, ",num_threads);
    printf ("elapsed time = %.6f seconds\n",end_time-start_time);
    printf ("pairs checked = %d\n",pairs_checked);        
    printf ("Extreme Distance = %.2f\n",sqrt(max_dist_sq));
    printf ("Extreme Pair = %d %d\n",extreme[0],extreme[1]);

    // free memory allocated for dataset 
    free(data);
}
