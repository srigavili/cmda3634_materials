#include <stdio.h> 
#include <stdlib.h> 
#include <omp.h>
#include <unistd.h> // for the usleep function

int main() {
    int N = 8;
    int num_threads = 2;
    omp_set_num_threads(num_threads);

    // start the timer
    double start_time, end_time;
    start_time = omp_get_wtime();
    
#pragma omp parallel
    {
        int thread_num = omp_get_thread_num();

#pragma omp for schedule(static)
        for (int i = 0; i < N; i++) {
	    printf ("thread  %d starting term %d\n",thread_num,i);
	    usleep (1000000*(N-i)); // wait N-i seconds
        }
    }

    // stop the timer
    end_time = omp_get_wtime();

    // output the results
    printf ("elapsed time = %.0f seconds\n",end_time-start_time);
}
