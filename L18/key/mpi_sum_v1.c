#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>

int main(int argc, char **argv) {

    MPI_Init (&argc, &argv);

    // MPI_COMM_WORLD is the default communicator that contains all ranks
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    char node_name[MPI_MAX_PROCESSOR_NAME];
    int node_name_len;
    MPI_Get_processor_name(node_name,&node_name_len);

    // make sure we are not running on a login node!
    if ((strcmp(node_name,"tinkercliffs1") == 0) || 
	(strcmp(node_name,"tinkercliffs2") == 0)) {
	printf ("error : running on login node %s!\n",node_name);
	return 1;
    }

    // get N from command line
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    long long N = atoll(argv[1]);

    // start the timer
    double start_time, end_time;
    start_time = MPI_Wtime();

    // calculate the sum
    long long sum = 0;
    for (long long i = 1; i <= N;i++) {
        sum += i;
    }

    // stop the timer
    end_time = MPI_Wtime();

    // print results
    printf ("rank %d (of %d) sum = %lld, N*(N+1)/2 = %lld\n",rank,size,sum,(N/2)*(N+1));
    printf ("rank %d elapsed time = %.4f seconds\n",rank,end_time-start_time);

    MPI_Finalize();

}
