#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 5
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=8
#SBATCH -o mpi_sum_timing.out

# check to make sure we provided a command line argument for N
if [ "$#" -ne 1 ]
then
  echo "error: please specify N as a command line argument"
  exit 1
fi

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules we need for MPI
module load matplotlib

# Build the executable
mpicc -DTIMING -o mpi_sum mpi_sum.c

# Run strong scaling study
mpiexec -n 1 --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_sum $1
mpiexec -n 2 --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_sum $1
mpiexec -n 4 --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_sum $1
mpiexec -n 8 --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_sum $1

# print newline
printf '\n'
