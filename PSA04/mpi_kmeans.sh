#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 5
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=8
#SBATCH -o mpi_kmeans.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules
module load matplotlib

# Build the executable
mpicc -o mpi_kmeans mpi_kmeans.c vec.c

# run mpi_kmeans
mpiexec -n $SLURM_NTASKS --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_kmeans $1 $2
