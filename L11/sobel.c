#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef unsigned char byte;

void sobel(byte A_pad[], byte E[], int rows, int cols, int threshold) {

    clock_t begin = clock();

    // Add code here to implement the Sobel edge detector

    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf ("Time to Run Nested For Loop Sobel Edge Detector in C = %.6lf seconds\n",
            time_spent);
}
