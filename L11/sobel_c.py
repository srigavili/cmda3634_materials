import sys # for command line arguments
import numpy as np # for matrix processing
from PIL import Image, ImageOps # for image processing
import ctypes as ct # for calling C from Python
lib = ct.cdll.LoadLibrary("./sobel.so") # load C sobel edge detector

# make sure command line arguments are provided
if (len(sys.argv) < 4):
    print ('command usage :',sys.argv[0],'infile','outfile','threshold')
    exit(1)
infile = sys.argv[1]
outfile = sys.argv[2]
threshold = int(sys.argv[3])

# open the input image, convert to grayscale,
# convert to a matrix, and determine the rows and cols
input = Image.open(infile)
gray = ImageOps.grayscale(input)
A = np.array(gray)
rows,cols = A.shape

# pad the perimeter of the image matrix with zeros
# and create the E matrix initialized to all zeros
A_pad = np.pad(A,[1,1],'constant',constant_values = 0)
E = np.zeros((rows,cols),dtype='uint8')

# call the C sobel edge detector
A_pad_cptr = A_pad.ctypes.data_as(ct.POINTER(ct.c_uint8))
E_cptr = E.ctypes.data_as(ct.POINTER(ct.c_uint8))
lib.sobel(A_pad_cptr,E_cptr,ct.c_int(rows),ct.c_int(cols),ct.c_int(threshold))

# create the edges image
edges = Image.fromarray(E)
edges.save(outfile)
