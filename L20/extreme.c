#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "vec.h"

int main (int argc, char* argv[]) {

    // read the filename from the command line
    if (argc < 2) {
        printf ("command usage: %s %s\n",argv[0],"filename");
        return 1;
    }
    char* filename = argv[1];

    // open the text file for reading
    FILE* fptr;
    fptr = fopen(filename,"r");

    // need to check for null
    if (fptr == 0) {
        printf ("Error opening data file %s.\n",filename);
        exit(1);
    }

    // read the number of points and the dimension of each point
    int num_points, dim;
    if (fscanf(fptr,"%*c %d %d",&num_points, &dim) != 2) {
        printf ("error reading the number of points and the dimension\n");
        return 1;
    }

    // Read vectors from stdin and store them in a 2d array
    double* data = (double*)malloc(num_points*dim*sizeof(double));
    if (data == NULL) {
        printf ("malloc return NULL pointer!\n");
        return 1;
    }
    for (int i=0;i<num_points;i++) {
        if (vec_read_file(fptr,data+i*dim,dim) != dim) {
            printf ("error reading the next point from the file %s\n",filename);
            return 1;
        }
    }

    // close the data file
    fclose(fptr);

    // start the timer 
    clock_t start_time = clock();

    // find the extreme pair
    double max_dist_sq = 0;
    int extreme[2];
    int pairs_checked = 0;    
    for (int i=0;i<num_points-1;i++) {
        for (int j=i+1;j<num_points;j++) {
            double dist_sq = vec_dist_sq(data+i*dim,data+j*dim,dim);
            pairs_checked += 1;	    
            if (dist_sq > max_dist_sq) {
                max_dist_sq = dist_sq;
                extreme[0] = i;
                extreme[1] = j;
            }
        }
    }

    // stop the timer and calculate elapsed time
    clock_t end_time = clock();
    double elapsed_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;

    // output the results
    printf ("elapsed time = %.4f seconds\n",elapsed_time);
    printf ("pairs checked = %d\n",pairs_checked);    
    printf ("Extreme Distance = %.2f\n",sqrt(max_dist_sq));
    printf ("Extreme Pair = %d %d\n",extreme[0],extreme[1]);

    // free memory allocated for dataset 
    free(data);
}
