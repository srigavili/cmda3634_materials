#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 5
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=4
#SBATCH -o mpi_extreme_rand.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules we need for MPI
module load matplotlib

# Build the executable
mpicc -O3 -o mpi_extreme_rand mpi_extreme_rand.c vec.c -lm

# run mpi_work on nodes*ntasks-per-node cores
mpiexec -n $SLURM_NTASKS --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_extreme_rand $1 $2 $3

