import numpy as np
import ctypes as ct # for calling C from Python
lib = ct.cdll.LoadLibrary("./matvec.so") # load C matvec function

# create 2d array of doubles
A = np.array([[0.2,1,2],[1,2.5,3],[4,5.8,6],[7,8,9.3]],dtype='float64')
(rows,cols) = A.shape
x = np.array([1,1,1],dtype='float64')
b = np.empty(4,dtype='float64');

# call C to do the matvec multiply
A_cptr = A.ctypes.data_as(ct.POINTER(ct.c_double))
x_cptr = x.ctypes.data_as(ct.POINTER(ct.c_double))
b_cptr = b.ctypes.data_as(ct.POINTER(ct.c_double))
lib.matvec(A_cptr,ct.c_int(rows),ct.c_int(cols),x_cptr,b_cptr)

# print result
print (b)
