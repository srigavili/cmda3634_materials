import numpy as np

# create 2d array of doubles
A = np.array([[0.2,1,2],[1,2.5,3],[4,5.8,6],[7,8,9.3]],dtype='float64')
(rows,cols) = A.shape
x = np.array([1,1,1],dtype='float64')
b = np.empty(4,dtype='float64');

# matvec multiply
for i in range(rows):
    b[i] = 0
    for j in range(cols):
        b[i] += A[i][j]*x[j]

# print result
print (b)
