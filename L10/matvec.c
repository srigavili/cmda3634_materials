void matvec(double A[], int rows, int cols, double x[], double b[]) {
    for (int i=0;i<rows;i++) {
        b[i] = 0;
        for (int j=0;j<cols;j++) {
            b[i] += A[i*cols+j]*x[j];
        }
    }
}
