#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include "vec.h"

int main (int argc, char* argv[]) {

    MPI_Init (&argc, &argv);

    // MPI_COMM_WORLD is the default communicator that contains all ranks
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    char node_name[MPI_MAX_PROCESSOR_NAME];
    int node_name_len;
    MPI_Get_processor_name(node_name,&node_name_len);

    // make sure we are not running on a login node!
    if ((strcmp(node_name,"tinkercliffs1") == 0) || 
            (strcmp(node_name,"tinkercliffs2") == 0)) {
        printf ("error : running on login node %s!\n",node_name);
        return 1;
    }

    // read the filename from the command line
    if (argc < 2) {
        printf ("command usage: %s %s\n",argv[0],"filename");
        return 1;
    }
    char* filename = argv[1];

    // open the text file for reading
    FILE* fptr;
    fptr = fopen(filename,"r");

    // need to check for null
    if (fptr == 0) {
        printf ("Error opening data file %s.\n",filename);
        exit(1);
    }

    // read the number of points and the dimension of each point
    int num_points, dim;
    if (fscanf(fptr,"%*c %d %d",&num_points, &dim) != 2) {
        printf ("error reading the number of points and the dimension\n");
        return 1;
    }

    // Read vectors from the file and store them in a 2d array
    double* data = (double*)malloc(num_points*dim*sizeof(double));
    if (data == NULL) {
        printf ("malloc return NULL pointer!\n");
        return 1;
    }
    for (int i=0;i<num_points;i++) {
        if (vec_read_file(fptr,data+i*dim,dim) != dim) {
            printf ("error reading the next point from the file %s\n",filename);
            return 1;
        }
    }

    // close the data file
    fclose(fptr);

    // start the timer
    double start_time, end_time;
    start_time = MPI_Wtime();

    // find the extreme pair
    double max_dist_sq = 0;
    int extreme[2];
    int pairs_checked = 0;    
    for (int i=0+rank;i<num_points-1;i+=size) {
        for (int j=i+1;j<num_points;j++) {
            double dist_sq = vec_dist_sq(data+i*dim,data+j*dim,dim);
            pairs_checked += 1;	    
            if (dist_sq > max_dist_sq) {
                max_dist_sq = dist_sq;
                extreme[0] = i;
                extreme[1] = j;
            }
        }
    }

    // reduce pairs_checked and max_dist_sq with result on rank 0
    int rank_pairs_checked = pairs_checked;
    struct { double dist_sq; int rank; } rank_max = { max_dist_sq, rank };
    struct { double dist_sq; int rank; } final_max;
    MPI_Reduce(&rank_pairs_checked,&pairs_checked,1,MPI_INT,MPI_SUM,0,MPI_COMM_WORLD);
    MPI_Reduce(&rank_max,&final_max,1,MPI_DOUBLE_INT,MPI_MAXLOC,0,MPI_COMM_WORLD);

    // stop the timer
    end_time = MPI_Wtime();

    // output the results
    if (rank == 0) {
        printf ("elapsed time = %.4f seconds\n",end_time-start_time);
        printf ("pairs checked = %d\n",pairs_checked);
        printf ("Extreme Distance = %.2f\n",sqrt(final_max.dist_sq));
	printf ("rank %d found extreme pair\n",final_max.rank);
	//        printf ("Extreme Pair = %d %d\n",extreme[0],extreme[1]);
    }

    // free memory allocated for dataset 
    free(data);

    MPI_Finalize();
}
