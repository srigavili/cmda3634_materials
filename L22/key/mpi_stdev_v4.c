#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>

int main (int argc, char* argv[]) {

    MPI_Init (&argc, &argv);

    // MPI_COMM_WORLD is the default communicator that contains all ranks
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    char node_name[MPI_MAX_PROCESSOR_NAME];
    int node_name_len;
    MPI_Get_processor_name(node_name,&node_name_len);

    // make sure we are not running on a login node!
    if ((strcmp(node_name,"tinkercliffs1") == 0) || 
            (strcmp(node_name,"tinkercliffs2") == 0)) {
        printf ("error : running on login node %s!\n",node_name);
        return 1;
    }

    // get N from the command line
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    long long N = atoll(argv[1]);

    // start the timer
    double start_time, end_time;
    start_time = MPI_Wtime();

    // each rank computes a partial sum
    long long sum = 0;
    for (long long i=1+rank;i<=N;i+=size) {
        sum += i;
    }

    // rank 0 sums up partial sums from each rank
    // rank 0 broadcasts the total sum to all other ranks
    // the MPI_IN_PLACE option is available for MPI_Allreduce but not MPI_Reduce!
    MPI_Allreduce(MPI_IN_PLACE,&sum,1,MPI_LONG_LONG,MPI_SUM,MPI_COMM_WORLD);

    // every rank has the correct sum and can now compute the mean
    double mean = 1.0*sum/N;

    // each rank computes a partial sum of difference squares
    double sum_diff_sq = 0;
    for (long long i=1+rank;i<=N;i+=size) {
        sum_diff_sq += (i-mean)*(i-mean);
    }

    // rank 0 sums up partial sums of difference squares from each rank
    double rank_sum_diff_sq = sum_diff_sq;
    MPI_Reduce(&rank_sum_diff_sq,&sum_diff_sq,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);

    // only rank 0 has the correct sum of diff sqs
    // and can now compute the correct variance
    // (other ranks will not compute the correct variance)
    double variance = sum_diff_sq/N;

    // calculate the standard deviation
    double stdev = sqrt(variance);

    // stop the timer
    end_time = MPI_Wtime();

    // only rank 0 has the correct standard deviation
    if (rank == 0) {
        printf ("num ranks = %d, elapsed time = %g\n",size,end_time-start_time);
        printf ("standard deviation is %.3lf, sqrt((N^2-1)/12) is %.3lf\n",
                stdev,sqrt((N*N-1)/12.0));
    }
    
    MPI_Finalize();
}

