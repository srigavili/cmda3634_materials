#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include "vec.h"

int main (int argc, char* argv[]) {

    MPI_Init (&argc, &argv);

    // MPI_COMM_WORLD is the default communicator that contains all ranks
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    char node_name[MPI_MAX_PROCESSOR_NAME];
    int node_name_len;
    MPI_Get_processor_name(node_name,&node_name_len);

    // make sure we are not running on a login node!
    if ((strcmp(node_name,"tinkercliffs1") == 0) || 
            (strcmp(node_name,"tinkercliffs2") == 0)) {
        printf ("error : running on login node %s!\n",node_name);
        return 1;
    }

    // read the filename from the command line
    if (argc < 2) {
        printf ("command usage: %s %s\n",argv[0],"filename");
        return 1;
    }
    char* filename = argv[1];

    // open the text file for reading
    FILE* fptr;
    fptr = fopen(filename,"r");

    // need to check for null
    if (fptr == 0) {
        printf ("Error opening data file %s.\n",filename);
        exit(1);
    }

    // read the number of points and the dimension of each point
    int num_points, dim;
    if (fscanf(fptr,"%*c %d %d",&num_points, &dim) != 2) {
        printf ("error reading the number of points and the dimension\n");
        return 1;
    }

    // Read vectors from the file and store them in a 2d array
    double* data = (double*)malloc(num_points*dim*sizeof(double));
    if (data == NULL) {
        printf ("malloc return NULL pointer!\n");
        return 1;
    }
    for (int i=0;i<num_points;i++) {
        if (vec_read_file(fptr,data+i*dim,dim) != dim) {
            printf ("error reading the next point from the file %s\n",filename);
            return 1;
        }
    }

    // close the data file
    fclose(fptr);

    // start the timer
    double start_time, end_time;
    start_time = MPI_Wtime();

    // calculate number of pairs per rank
    // assume num_points is even
    int num_pairs = (num_points/2)*(num_points-1);
    int rank_pairs = (num_pairs+size-1)/size;
    int start = rank_pairs*rank;
    int end = start+rank_pairs;
    if (end > num_pairs) {
	end = num_pairs;
    }

    // find the extreme pair
    double max_dist_sq = 0;
    int extreme[2];
    int j = floor(sqrt(2*(double)start+0.25)+0.5);
    int j_choose_2;
    if (j % 2 == 0) {
	j_choose_2 = (j/2)*(j-1);
    } else {
	j_choose_2 = j*((j-1)/2);
    }
    int i = start - j_choose_2;
    for (int k=start;k<end;k++) {
	double dist_sq = vec_dist_sq(data+i*dim,data+j*dim,dim);
	if (dist_sq > max_dist_sq) {
	    max_dist_sq = dist_sq;
	    extreme[0] = i;
	    extreme[1] = j;
	}
	// increment pair
	i += 1;
	if (i==j) {
	    j += 1;
	    i = 0;
	}
    }
    
    // gather up extreme pairs from each rank and give to rank 0
    int extreme_pairs[2*size];
    MPI_Gather(extreme,2,MPI_INT,extreme_pairs,2,MPI_INT,0,MPI_COMM_WORLD);
    
    // rank 0 finds the most extreme pair
    if (rank == 0) {
	for (int k=1;k<size;k++) {
	    int i = extreme_pairs[2*k];
	    int j = extreme_pairs[2*k+1];
	    double dist_sq = vec_dist_sq(data+i*dim,data+j*dim,dim);
	    if (dist_sq > max_dist_sq) {
		max_dist_sq = dist_sq;
		extreme[0] = i;
		extreme[1] = j;
	    }
	}
    }

    // stop the timer
    end_time = MPI_Wtime();

    // output the results
    if (rank == 0) {
        printf ("elapsed time = %.4f seconds\n",end_time-start_time);
        printf ("Extreme Distance = %.2f\n",sqrt(max_dist_sq));
	printf ("Extreme Pair = %d %d\n",extreme[0],extreme[1]);
    }

    // free memory allocated for dataset 
    free(data);

    MPI_Finalize();
}
