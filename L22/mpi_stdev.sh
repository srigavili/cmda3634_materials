#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 5
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=4
#SBATCH -o mpi_stdev.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules we need for MPI
module load matplotlib

# Build the executables
mpicc -o mpi_stdev mpi_stdev.c -lm

# run mpi standard deviation
printf 'Results with 1 rank:\n'
mpiexec -n 1 --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_stdev $1
printf '\nResults with 4 ranks:\n'
mpiexec -n $SLURM_NTASKS --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_stdev $1
