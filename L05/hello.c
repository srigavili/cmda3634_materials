#include <stdio.h>
#include <string.h>

int main () {
    char str[] = "Hello World!";
    int lower = 0;
    for (int i=0;i<strlen(str);i++) {
        if ((str[i] >= 'a') && (str[i] <= 'z')) {
            lower += 1;
        }
    }
    printf ("The string %s contains %d lower case letters\n",str,lower);
}
