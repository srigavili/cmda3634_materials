#include <stdio.h>

int main () {
    int a[3] = { 1, 2, 3};
    int b[4] = { 1, 2 }; // missing elements initialized to 0
    int c[5] = { 0 }; // initialize all elements to 0
    int d[] = { 1, 2, 3, 4, 5, 6 }; // size of array inferred
    printf ("a = [ %d %d %d ]\n\n",a[0],a[1],a[2]);
    printf ("b = [ %d %d %d %d ]\n\n",b[0],b[1],b[2],b[3]);
    printf ("c = [ %d %d %d %d %d ]\n\n",c[0],c[1],c[2],c[3],c[4]);
    printf ("d = [ %d %d %d %d %d %d]\n\n",d[0],d[1],d[2],d[3],d[4],d[5]);
}
