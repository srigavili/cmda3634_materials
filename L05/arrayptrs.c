#include <stdio.h>

int main () {
    int a[3] = { 1, 2, 3 };
    printf ("a = [ %d %d %d ]\n\n",a[0],a[1],a[2]);

    int* b = a; // b is an integer pointer that points to the beginning of a
    *b = 4;
    printf ("a = [ %d %d %d ]\n\n",a[0],a[1],a[2]);

    b[1] = 5; // we can also use the "array syntax" for the pointer b!
    printf ("a = [ %d %d %d ]\n\n",a[0],a[1],a[2]);

    *(b+2) = 6; // using "pointer arithmetic" -> equivalent to b[2] = 6
    printf ("a = [ %d %d %d ]\n\n",a[0],a[1],a[2]);

}
