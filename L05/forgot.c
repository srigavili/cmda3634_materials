#include <stdio.h>
#include <string.h>

int main () {
    char str1[] = { 'H', 'e', 'l', 'l', 'o' };
    char str2[] = "other stuff";
    printf ("length of str1 is %ld\n",strlen(str1));
    printf ("length of str2 is %ld\n",strlen(str2));
}
