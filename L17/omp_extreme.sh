#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 10
#SBATCH --cpus-per-task=32
#SBATCH -o omp_extreme.out

# check to make sure we provided a command line argument for the filename
if [ "$#" -ne 1 ]
then
  echo "error: please specify filename as a command line argument"
  exit 1
fi

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules (including the gcc compiler!)
module load matplotlib

# compile using TIMING flag
gcc -DTIMING -DDYNAMIC -O3 -o omp_extreme omp_extreme.c vec.c -lm -fopenmp

# OpenMP settings
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export OMP_PROC_BIND=true

# run multiple times for scaling study
cat $1 | ./omp_extreme 1
cat $1 | ./omp_extreme 2
cat $1 | ./omp_extreme 4
cat $1 | ./omp_extreme 8
cat $1 | ./omp_extreme 16
cat $1 | ./omp_extreme 32
