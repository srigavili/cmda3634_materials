#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <unistd.h> // for the usleep function

int main(int argc, char* argv[]) {

    MPI_Init (&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&size);

    // get number of seconds to sleep from command line
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"time");
        return 1;
    }
    int time = atoi(argv[1]);

    printf ("MPI rank %d, number of ranks = %d taking a %d second nap!\n",
	    rank,size,time);    
    usleep (1000000*time); // wait time seconds
    printf ("MPI rank %d, number of ranks = %d woke up!\n",rank,size);    

    MPI_Finalize();
}
