#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p p100_normal_q
#SBATCH -t 5
#SBATCH --gres=gpu:1
#SBATCH -o gpu_sum.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load CUDA toolkit module
module load cuda11.6/toolkit/11.6.2

# compile
nvcc -w -arch=sm_60 -o gpu_sum gpu_sum.cu

# run sum
./gpu_sum $1 $2

