#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

typedef unsigned long long int uint64;

__global__ void sumKernel(uint64 N) {

    int thread_num = threadIdx.x;
    int num_threads = blockDim.x;

    uint64 sum = 0;
    for (uint64 i = 1+thread_num; i <= N;i+=num_threads) {
        sum += i;
    }

    printf (" on thread %d of %d, sum = %llu\n",thread_num,num_threads,sum);
}

int main(int argc, char **argv) {

    /* get N and num_threads from the command line */
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"N","num_threads");
        return 1;
    }

    uint64 N = atol(argv[1]);
    int num_threads = atoi(argv[2]);

    printf ("num_threads = %d\n",num_threads);
    printf ("N*(N+1)/2 = %llu\n",(N/2)*(N+1));

    sumKernel <<< 1, num_threads >>> (N);
    cudaDeviceSynchronize();

}
