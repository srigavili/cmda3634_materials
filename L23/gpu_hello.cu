#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

__global__ void helloKernel() {

    /*****************/
    /* Add Code Here */
    /*****************/

}

int main(int argc, char **argv) {

    /* get num_threads from the command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"num_threads");
        return 1;
    }

    int num_threads = atoi(argv[1]);

    printf ("num_threads = %d\n",num_threads);

    helloKernel <<< 1, num_threads >>> ();
    cudaDeviceSynchronize();
}
