#include <stdio.h>
#include <stdlib.h>

int main (int argc, char* argv[]) {
    if (argc < 3) {
        printf ("command usage: %s %s %s\n",argv[0],"n","k");
        return 1;
    }
    int n = atoi(argv[1]);
    int k = atoi(argv[2]);
    long long answer = 1;
    for (int i=0;i<=k-1;i++) {
        answer = answer*(n-i)/(i+1);
    }
    printf ("%lld\n",answer);
}
