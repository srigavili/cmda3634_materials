#include <stdio.h>

int main () {
    int next;
    int sum = 0;
    int count = 0;
    while (scanf("%d",&next) == 1) {
        sum += next;
        count += 1;
    }
    double average = (double)sum/count;
    printf ("average = %.2f\n",average);
}
