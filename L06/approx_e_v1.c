#include <stdio.h>
#include <stdlib.h>

int main (int argc, char* argv[]) {
    if (argc < 2) {
        printf ("command usage: %s %s\n",argv[0],"n");
        return 1;
    }
    int n = atoi(argv[1]);
    float approx_e = 0.;
    long long fact = 1;
    for (int i=1;i<=n;i++) {
        approx_e += 1.0/fact;
        fact *= i;
    }
    printf ("exact  value of e is %.15f\n",2.718281828459045);
    printf ("approx value of e is %.15f\n",approx_e);
}
