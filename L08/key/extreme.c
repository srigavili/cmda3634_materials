#include <stdio.h>
#include <math.h>
#include "vec.h"

int main () {

    // read the number of points and the dimension of each point
    int num_points, dim;
    if (scanf("%*c %d %d",&num_points, &dim) != 2) {
        printf ("error reading the number of points and the dimension\n");
        return 1;
    }

    // Read vectors from stdin and store them in a 2d array
    double data[num_points][dim];
    for (int i=0;i<num_points;i++) {
        if (vec_read_stdin(data[i],dim) != dim) {
            printf ("error reading the next point from stdin\n");
            return 1;
        }
    }

    // find the extreme pair
    double max_dist_sq = 0;
    int extreme1, extreme2;
    for (int i=0;i<num_points-1;i++) {
        for (int j=i+1;j<num_points;j++) {
            double dist_sq = vec_dist_sq(data[i],data[j],dim);
            if (dist_sq > max_dist_sq) {
                max_dist_sq = dist_sq;
                extreme1 = i;
                extreme2 = j;
            }
        }
    }

    // output the results
    printf ("Extreme Distance = %.2f\n",sqrt(max_dist_sq));
    printf ("Extreme Pair = %d %d\n",extreme1,extreme2);
}
