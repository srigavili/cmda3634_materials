#include <stdio.h>

int main () {
    double A[3][3] = { { 1, 2.5, 3 }, { 4, 5.8, 6 }, { 7, 8, 9.3 } };
    double x[3] = { 1, 1, 1 };
    double b[3] = { 0 };
    for (int i=0;i<3;i++) {
        for (int j=0;j<3;j++) {
            b[i] += A[i][j]*x[j];
        }
    }
    printf ("b = { %.2f, %.2f, %.2f }\n",b[0],b[1],b[2]);
}
