#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p p100_normal_q
#SBATCH -t 5
#SBATCH --gres=gpu:1
#SBATCH -o gpu_collatz.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load CUDA toolkit module
module load cuda11.6/toolkit/11.6.2

# compile
nvcc -arch=sm_60 -o gpu_collatz gpu_collatz.cu

# run collatz
./gpu_collatz $1 $2

