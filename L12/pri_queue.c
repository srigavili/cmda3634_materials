#include <stdio.h>
#include <stdlib.h>
#include "pri_queue.h"

void pri_queue_init(pri_queue* pq, int max_size) {

    // Add code here

    // error checking for malloc
    if (pq->elements == NULL) {
	printf ("error : malloc failed in pri_queue_init\n");
	exit(1);
    }
}

pri_queue_element pri_queue_peek_top(pri_queue* pq) {
    if (pq->cur_size == 0) {
        printf ("error : underflow in pri_queue_peek_top\n");
        exit(1);
    }
    
    // Add code here
    
}

void pri_queue_delete_top(pri_queue* pq) {
    if (pq->cur_size == 0) {
        printf ("error : underflow in pri_queue_delete_top\n");
        exit(1);
    }

    // Add code here
    
}

void pri_queue_insert(pri_queue* pq, pri_queue_element element) {
    if (pq->cur_size >= pq->max_size) {
        printf ("error : overflow in pri_queue_insert\n");
        exit(1);
    }

    // find the correct place for the new element
    // while simultaneously moving existing elements with
    // higher priority to the right one space

    // Add code here 
    
    // insert the new element

    // Add code here
}

void pri_queue_free(pri_queue* pq) {
    if (pq->elements == 0) {
        printf ("error : null elements pointer in pri_queue_free\n");
        exit(1);
    }

    // Add code here
    
}
