#include <stdio.h>
#include <stdlib.h>
#include "vec.h"
#include "pri_queue.h"

int main (int argc, char* argv[]) {

    // read the command line arguments
    if (argc < 3) {
        printf ("command usage: %s %s %s\n",argv[0],"k","index");
        return 1;
    }
    int k = atoi(argv[1]);
    int index = atoi(argv[2]);

    // read the number of points and the dimension of each point
    int num_points, dim;
    if (scanf("%*c %d %d",&num_points, &dim) != 2) {
        printf ("error reading the number of points and the dimension\n");
        return 1;
    }

    // Read vectors from stdin and store them in a flat 2d array
    double* data = (double*)malloc(num_points*dim*sizeof(double));
    if (data == NULL) {
        printf ("failed to allocate data array!\n");
        return 1;
    }
    for (int i=0;i<num_points;i++) {
        // data+i*dim is a pointer to the ith data point
        if (vec_read_stdin(data+i*dim,dim) != dim) {
            printf ("error reading the next point from stdin\n");
            return 1;
        }
    }

    // initialize the priority queue
    pri_queue pq;
    pri_queue_init (&pq,k);

    // calculate all distances squared
    // and store the indices of the k nearest in the priority queue

    // *************
    // Add code here
    // *************
    
    // remove the k nearest indices from the priority queue
    int nearest[k];
    for (int i=k-1;i>=0;i--) {
        pri_queue_element top = pri_queue_peek_top(&pq);
        nearest[i] = top.id;
        pri_queue_delete_top(&pq);
    }

    // print the k nearest indices from closest to farthest
    printf ("k nearest neighbors: ");
    for (int i=0;i<k;i++) {
        printf ("%d ",nearest[i]);
    }
    printf ("\n");

    // free the priority queue */
    pri_queue_free (&pq);

    // free our heap array
    free (data);
}

