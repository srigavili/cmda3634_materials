#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 120
#SBATCH -o harmonic.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load CUDA toolkit module
module load matplotlib

# compile
gcc -O3 -o harmonic harmonic.c

# run harmonic
./harmonic $1

