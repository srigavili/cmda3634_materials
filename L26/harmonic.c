#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef unsigned long long int uint64;

int main(int argc, char **argv) {
    double term;
    double sum = 0;

    // get N from command line
    if (argc != 2) {
	printf ("Command usage : %s %s\n",argv[0],"N");
	return 1;
    }
    uint64 N = atoll(argv[1]);

    // start the timer
    clock_t start = clock();

    // calculate sum
    for (uint64 i = 1; i <= N; i++) {
	sum += 1.0/i;
    }

    // stop the timer
    clock_t stop = clock();
    double elapsed = (double)(stop-start)/CLOCKS_PER_SEC;

    // print results
    printf ("sum = %.10f\n",sum);
    printf ("elapsed time = %.2f seconds\n",elapsed);
}
